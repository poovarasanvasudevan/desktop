import './preload-events';
import './preload-listeners';
import './preload-notification';

// Let the inside app know it's a desktop app
window.isDesktop = true;
